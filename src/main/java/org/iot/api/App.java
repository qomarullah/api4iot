package org.iot.api;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mchange.v2.codegen.bean.Property;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
*/
public class App 
{
	private final static Logger log = LogManager.getLogger(App.class);
	public static Properties prop;
	public static String test="1";
	
    public static void main( String[] args )
    {
    	//setup properties
    	prop = new Properties();
    	if(args.length>0)
    		loadProperties(prop, args[0]);
    	else{
    		log.debug("properties not found");
    		System.exit(0);
    	}
    	
    	//setup log
        if(args.length>1)
    		BasicConfigurator.configure();
    	else
    		PropertyConfigurator.configure(args[0]);
    	
    	
    	//setup server
    	int port=Integer.parseInt(prop.getProperty("server.port","9001"));
    	int maxThreads = Integer.parseInt(prop.getProperty("server.maxthread","200"));
    	int minThreads = Integer.parseInt(prop.getProperty("server.minthread","30"));
    	int timeOutMillis = Integer.parseInt(prop.getProperty("server.timeout","20000"));
    	
    	
    	
    	String urlStatus=prop.getProperty("url.status","http://174.138.28.157/transport/getStatus.php");
    	String urlUpdate=prop.getProperty("url.update","http://174.138.28.157/transport/updateStatus.php");
    	String macDevice=prop.getProperty("device.mac","D8B04CFD0929");
    	String pwdDevice=prop.getProperty("device.pwd","admin");
    	String ipDevice=prop.getProperty("device.ip","192.168.43.145");
    	int portDevice=Integer.parseInt(prop.getProperty("device.port","8899"));
    	int delayDevice=Integer.parseInt(prop.getProperty("device.delay","5000"));
    	
    	
    	//start server
    	new SparkServer(port,minThreads,maxThreads,timeOutMillis);
    	
    	log.debug("Start Refresh Update");
    	//public ThreadUpdate(String url, String mac, String pwd, String ip, int port) {
    	
        ThreadUpdate threadUpdate = new ThreadUpdate(urlStatus,urlUpdate, macDevice, pwdDevice, ipDevice, portDevice,delayDevice);
        new Thread(threadUpdate).start();
    }
    
    public static void loadProperties(Properties prop, String filename){

    	InputStream input = null;
    	try {

    		input = new FileInputStream(filename);
    		// load a properties file
    		prop.load(input);
    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}

    }
}
