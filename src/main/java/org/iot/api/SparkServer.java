package org.iot.api;

/**
 * Bismillahirrahmanirrahim
 * @author Qomarullah
 * @time 9:27:44 PM
 */

import static spark.Spark.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
*/
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iot.bridge.BaseConnect;
import org.iot.bridge.Device;
import org.iot.bridge.LocalConnect;
import org.iot.bridge.StringUtil;

import com.fasterxml.jackson.databind.ObjectMapper;


public class SparkServer {

	private final static Logger log = LogManager.getLogger(SparkServer.class);
	boolean isConnected = false;
	BaseConnect connect = null;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	public SparkServer(int port, int minThreads, int maxThreads, int timeOutMillis){
		
	
		port(port);
		threadPool(maxThreads, minThreads, timeOutMillis);
		get("/hello", (req, res) -> "Bismillahirrahmanirrahim");
		
		get("/set", (request, response) -> {
			String resp="";
			String status=request.queryParams("status");
			String mac=request.queryParams("mac");
			String pwd=request.queryParams("pwd");
			String ip=request.queryParams("ip");
			String portDevice=request.queryParams("port");
			
			//System.out.println(mac+"="+ pwd+"="+ip+"="+portDevice);
			int _portDevice=Integer.parseInt(portDevice);
			
		    try {
		    	if(!isConnected){
		    		//connect.destroy();
		    		/*try {
		    			connect.closeConnect();
			    			
					} catch (Exception e) {
						// TODO: handle exception
					}*/
		    		//Thread.sleep(1000);
		    		isConnected = tryConnect(mac, pwd, ip, _portDevice);
		    		Thread.sleep(1000);
		    	}
		    	boolean isSend= false;
		    	
		    	if(isConnected)
		    	{
		    		if(status.equals("on")) {
			    		byte[] data=getCtrlOutOpenCmd(1);
			            String x=StringUtil.bytesToHexString(data);
			            isSend = connect.send(data);
		    		}
			            //System.out.println(isSend);
			            Thread.sleep(1000);
		    		if(status.equals("off")) {
			    		byte[] data2=getCtrlOutCloseCmd(1);
			            String x2=StringUtil.bytesToHexString(data2);
			            isSend = connect.send(data2);
			                
		    		}
		    	}
		    	resp=isSend+"";
			} catch (Exception e) {
				// TODO: handle exception
				resp = e.getMessage();
			}
		    log.info("Switch-"+status+":"+resp);
		    return resp;
		});
		
	}

	
public boolean tryConnect(String mac, String pasd, String ip, int port) {

	/*String mac="D8B04CFD0929";
    String pasd="admin";
    String ip="10.10.100.254";
    int port=8899;*/
	Device device=new Device();
	device.setMac(mac);
	device.setPasd(pasd);
	device.setIp(ip);
	device.setPort(port);
	
	int timeOut=30000;

	 //lc.connect(1000);
     //System.out.println("======="+lc.isConnect());
     
 	
    connect = new LocalConnect(ip, mac, pasd, port, true);
	
    connect.setrListener(new BaseConnect.ReceiveDataListener()
    {
    	  public void onReceive(byte[] data, String mac)
          {
            log.info("============================receive:"+mac);
          }
          
          public void connectSuccess(String mac)
          {
        	  log.info("============================connect-receive:"+mac);
	          
          }
          
          public void connectBreak(String info, String mac)
          {
        	  log.info("============================connect-break:"+info);
	          
          }
	    
	 });
    
    isConnected = connect.connect(timeOut);
    return isConnected;
}


public static byte[] getCtrlOutCloseCmd(int position) {
	byte[] key = new byte[3];
	key[0] = 0x00;
	key[1] = 0x01;
	key[2] = (byte) (position & 0xff);
	return generateCmd(key);

}


public static byte[] getCtrlOutOpenCmd(int position) {
	byte[] key = new byte[3];
	key[0] = 0x00;
	key[1] = 0x02;
	key[2] = (byte) (position & 0xff);
	return generateCmd(key);
}
public static byte[] generateCmd(byte[] key) {
	// ����GPIOЭ�飺��ͷ2 + ���� 2 + ID1 + ���� + ����+ У��1
	int length = 5 + key.length;
	byte[] cmd = new byte[length];
	cmd[0] = 0x55;
	cmd[1] = (byte) 0xaa;

	// GPIOЭ���еĳ���
	byte[] lengthBytes = int2byte(key.length);
	cmd[2] = lengthBytes[1];
	cmd[3] = lengthBytes[0];

	// У��λ�ۼ�
	cmd[length - 1] = (byte) (cmd[2] + cmd[3]);
	for (int i = 0; i < key.length; i++) {
		cmd[i + 4] = key[i];
		cmd[length - 1] += key[i];
	}
	return cmd;
}
public static byte[] int2byte(int res) {
	byte[] targets = new byte[4];
	targets[0] = (byte) (res & 0xff);// ���λ
	targets[1] = (byte) ((res >> 8) & 0xff);// �ε�λ
	targets[2] = (byte) ((res >> 16) & 0xff);// �θ�λ
	targets[3] = (byte) (res >>> 24);// ���λ,�޷������ơ�
	return targets;
}
	
}