package org.iot.api;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iot.bridge.BaseConnect;
import org.iot.bridge.Device;
import org.iot.bridge.LocalConnect;
import org.iot.bridge.StringUtil;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class ThreadUpdate extends TimerTask {
	
	protected static Logger logger = LogManager.getLogger(ThreadUpdate.class);
	   
	private volatile boolean isRunning = true;
	static ConcurrentHashMap<String, Long> hashLastUpdate = new ConcurrentHashMap<String, Long>();
	String serverid;
	String url;
	String url_update;
	
	String mac="D8B04CFD0929";
    String pwd="admin";
    String ip="192.168.43.145";
    int _portDevice=8899;
    int delay=5000;
   
	boolean isConnected = false;
	BaseConnect connect = null;
	
	public ThreadUpdate(String url_status, String url_update, String mac, String pwd, String ip, int port, int delay) {
		super();
		logger.info("RefreshThread instance created...");
		this.serverid = "one";
		//this.url="http://128.199.190.210/securezone/api/getStatus.php";
		this.url=url_status;
		this.url_update=url_update;
		this.mac=mac;
		this.pwd=pwd;
		this.ip=ip;
		this._portDevice=port;
		this.delay=delay;
	}

	public void stop() {
		isRunning = false;
	}
	 
	
	 
	public void run() {
		for (; isRunning;) {
			String urlCall=this.url;
			String urlUpdate=this.url_update;
			
			try {
				Thread.sleep(this.delay);
			} catch (InterruptedException e1) {
				System.out.println(e1.getLocalizedMessage());
			}
			
			String resp=get(urlCall, 10000);
			System.out.println(resp);
			
			//create ObjectMapper instance
			ObjectMapper objectMapper = new ObjectMapper();
			
			//convert json string to object
			HashMap<String, Object> result=null;
			try {
				result = objectMapper.readValue(resp, HashMap.class);
				
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(result!=null) {
				int count=(int) result.get("status");
				if(count > 0) {
					setLamp("on");
					
					String respUpdate=get(urlUpdate+"?status=0", 10000);
					System.out.println("submit off======>"+respUpdate);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
						System.out.println(e1.getLocalizedMessage());
					}
					setLamp("off");
				}else {
					setLamp("off");
				}
			}
			
			logger.info("Check update config()");
			
		}
	}
	
	
	public boolean setLamp(String status) {
		boolean isSend=false;
		/*String mac="D8B04CFD0929";
	    String pwd="admin";
	    String ip="192.168.43.145";
	    int _portDevice=8899;
	   */ 
		//System.out.println(mac+"="+ pwd+"="+ip+"="+_portDevice);
		//int _portDevice=Integer.parseInt(portDevice);
    	System.out.println("status connect======>"+isConnected+mac+";"+pwd+";"+ip+";"+ _portDevice);
		
	    try {
	    	if(!isConnected){
	    		isConnected = tryConnect(mac, pwd, ip, _portDevice);
	    		Thread.sleep(1000);
	    	}
	    	if(isConnected)
	    	{
	    		if(status.equals("on")) {
		    		byte[] data=getCtrlOutOpenCmd(1);
		            String x=StringUtil.bytesToHexString(data);
		            isSend = connect.send(data);
	    		}
		        if(status.equals("off")) {
		    		byte[] data2=getCtrlOutCloseCmd(1);
		            String x2=StringUtil.bytesToHexString(data2);
		            isSend = connect.send(data2);
		                
	    		}
		        System.out.println("send to device====>"+isSend);
		        
	    	}
	    } catch (Exception e) {
			// TODO: handle exception
	    	isSend=false;			
	    	logger.error(e.getMessage());
		}
	    return isSend;
	}
	public String get(String url, int timeout) {
	    return get(url, timeout, null,null);
	  }
	  public String get(String url, int timeout, String proxyHost,
	      String proxyPort) {
	    long start = System.currentTimeMillis();
	    url = url.replaceAll(" ", "+");
	    String response = null;
	    HttpClient http = new HttpClient();
	    
	    http.getParams().setSoTimeout(timeout);
	    if (proxyHost != null && proxyPort != null) {
	      logger.info("Using proxy = " + proxyHost + ":" + proxyPort);
	      http.getHostConfiguration().setProxy(proxyHost,
	          Integer.parseInt(proxyPort));
	    }
	    HttpMethod method = new GetMethod(url);
	    http.getParams().setParameter("http.socket.timeout", timeout);
	    http.getParams().setParameter("http.connection.timeout", timeout);
	    method.getParams().setParameter("http.socket.timeout", timeout);
	    method.getParams().setParameter("http.connection.timeout", timeout);
	    try {
	      int statusCode = http.executeMethod(method);
	      if (statusCode != HttpStatus.SC_OK && statusCode != 202) {
	        response = "HTTP_FAILED";
	      } else {
	        response = method.getResponseBodyAsString().trim();
	      }
	    } catch (Exception e) {
	      // e.printStackTrace();
	      logger.error("Error in httpGet", e);
	      response = "FAILED";
	    } finally {
	      method.releaseConnection();
	    }
	    
	    logger.info("HTTP-GET|" + url + "|Response|" + response + "|"+ (System.currentTimeMillis() - start) + "ms");

	    logger.debug("HTTP-GET|" + url + "|Response|" + response + "|"+ (System.currentTimeMillis() - start) + "ms");

	    return response;
	  }
	  

		
public boolean tryConnect(String mac, String pasd, String ip, int port) {

	/*String mac="D8B04CFD0929";
    String pasd="admin";
    String ip="10.10.100.254";
    int port=8899;*/
	
	Device device=new Device();
	device.setMac(mac);
	device.setPasd(pasd);
	device.setIp(ip);
	device.setPort(port);
	
	int timeOut=30000;

	 //lc.connect(1000);
     
 	
    connect = new LocalConnect(ip, mac, pasd, port, true);
	
    connect.setrListener(new BaseConnect.ReceiveDataListener()
    {
    	  public void onReceive(byte[] data, String mac)
          {
            System.out.println("============================receive:"+mac);
          }
          
          public void connectSuccess(String mac)
          {
        	  System.out.println("============================connect-receive:"+mac);
	          
          }
          
          public void connectBreak(String info, String mac)
          {
        	  logger.info("============================connect-break:"+info);
	          
          }
	    
	 });
    
    isConnected = connect.connect(timeOut);
    return isConnected;
}


public static byte[] getCtrlOutCloseCmd(int position) {
	byte[] key = new byte[3];
	key[0] = 0x00;
	key[1] = 0x01;
	key[2] = (byte) (position & 0xff);
	return generateCmd(key);

}


public static byte[] getCtrlOutOpenCmd(int position) {
	byte[] key = new byte[3];
	key[0] = 0x00;
	key[1] = 0x02;
	key[2] = (byte) (position & 0xff);
	return generateCmd(key);
}
public static byte[] generateCmd(byte[] key) {
	// ����GPIOЭ�飺��ͷ2 + ���� 2 + ID1 + ���� + ����+ У��1
	int length = 5 + key.length;
	byte[] cmd = new byte[length];
	cmd[0] = 0x55;
	cmd[1] = (byte) 0xaa;

	// GPIOЭ���еĳ���
	byte[] lengthBytes = int2byte(key.length);
	cmd[2] = lengthBytes[1];
	cmd[3] = lengthBytes[0];

	// У��λ�ۼ�
	cmd[length - 1] = (byte) (cmd[2] + cmd[3]);
	for (int i = 0; i < key.length; i++) {
		cmd[i + 4] = key[i];
		cmd[length - 1] += key[i];
	}
	return cmd;
}
public static byte[] int2byte(int res) {
	byte[] targets = new byte[4];
	targets[0] = (byte) (res & 0xff);// ���λ
	targets[1] = (byte) ((res >> 8) & 0xff);// �ε�λ
	targets[2] = (byte) ((res >> 16) & 0xff);// �θ�λ
	targets[3] = (byte) (res >>> 24);// ���λ,�޷������ơ�
	return targets;
}

}

