package org.iot.bridge;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


public abstract class BaseConnect extends Thread
{
  final String TAG = BaseConnect.class.getSimpleName();
  String host;
  String mac;
  String pasd;
  int port;
  Socket socket;
  OutputStream os;
  InputStream is;
  boolean close;
  boolean isLocal;
  ConnectStateChecker stateChecker;
  ReceiveDataListener rListener;
  
  public BaseConnect(String host, String mac, String pasd, int port, boolean isLocal)
  {
    this.host = host;
    this.mac = mac;
    this.pasd = pasd;
    this.port = port;
    this.isLocal = isLocal;
  }
  
  public abstract boolean connect(int paramInt);
  
  public void run()
  {
    while (!this.close)
    {
      System.out.println("start");
      if (!isConnect())
      {
    	  System.out.println("not-connect");
    	     
    	  this.close = true;
        
         closeConnect();
        if (this.rListener == null) {
        	System.out.println("rlistener-no");
      	  break;
        }
        this.rListener.connectBreak("error_002", this.mac);
        break;
      }
      try
      {
        if (this.is == null) {
          break;
        }
        byte[] data = new byte['?'];
        int length=0;
        
        try
        {
          length = this.is.read(data, 0, data.length);
          System.out.println(length);
        }
        catch (NullPointerException e)
        {
          //length;
          //Log.d(this.TAG, "------------>catch null");
          System.out.println("error");	
          e.printStackTrace();
          break;
        }
        if (length > 0)
        {
          byte[] tmp = new byte[length];
          System.arraycopy(data, 0, tmp, 0, length);
          if (this.rListener != null) {
            this.rListener.onReceive(tmp, this.mac);
          }
        }
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  public boolean closeConnect()
  {
    if (this.socket != null) {
      try
      {
        this.close = true;
        this.socket.close();
        if (this.os != null) {
          this.os.close();
        }
        if (this.is != null) {
          this.is.close();
        }
        this.socket = null;
        this.os = null;
        this.is = null;
        if (this.rListener != null) {
          this.rListener.connectBreak("Connect is closed", this.mac);
        }
        return true;
      }
      catch (IOException e)
      {
        if (this.rListener != null) {
          this.rListener.connectBreak("error_005", this.mac);
        }
        e.printStackTrace();
        this.close = false;
        return false;
      }
    }
    return true;
  }
  
  public boolean send(byte[] data)
  {
    if (!isConnect())
    {
      if (this.rListener != null) {
        this.rListener.connectBreak("error_006", this.mac);
      }
      return false;
    }
    try
    {
      this.os.write(data);
      this.os.flush();
    }
    catch (Exception e)
    {
      if (this.rListener != null) {
        this.rListener.connectBreak("error_007", this.mac);
      }
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  public boolean isConnect()
  {
    try
    {
      if ((this.stateChecker != null) && 
        (!this.stateChecker.isConnected())) {
        return false;
      }
      return (this.socket != null) && (this.socket.isConnected());
    }
    catch (Exception e) {}
    return false;
  }
  
  public String getMac()
  {
    return this.mac;
  }
  
  public void setLocal(boolean isLocal)
  {
    this.isLocal = isLocal;
  }
  
  public boolean isLocal()
  {
    return this.isLocal;
  }
  
  public void setrListener(ReceiveDataListener rListener)
  {
    this.rListener = rListener;
  }
  
  public static abstract interface ReceiveDataListener
  {
    public abstract void onReceive(byte[] paramArrayOfByte, String paramString);
    
    public abstract void connectSuccess(String paramString);
    
    public abstract void connectBreak(String paramString1, String paramString2);
  }
}