package org.iot.bridge;

import java.net.Socket;

public class ConnectStateChecker
  extends Thread
{
  private final String TAG = ConnectStateChecker.class.getSimpleName();
  private Socket socket;
  private boolean isConnected = true;
  private String mac;
  
  public ConnectStateChecker(Socket socket, String mac)
  {
    this.socket = socket;
    this.mac = mac;
  }
  
  public void run()
  {
    try
    {
      for (;;)
      {
        this.socket.sendUrgentData(255);
        Thread.sleep(3000L);
      }
    }
    catch (Exception e)
    {
      //Log.d(this.TAG, "------------>mac:" + this.mac + " connect has broken");
      this.isConnected = false;
      e.printStackTrace();
    }
  }
  
  public boolean isConnected()
  {
    return this.isConnected;
  }
}