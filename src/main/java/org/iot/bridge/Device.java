package org.iot.bridge;

import java.io.Serializable;

/**
 * ����һ���豸
 * 
 * @author liu jin qi
 */
public class Device implements Serializable {
	// �豸��
	private String name;
	// �豸mac
	private String mac;
	// �豸ip
	private String ip;
	// �豸����  -1��ʾԶ���豸
	private int type;
	// Ĭ������
	private String pasd = "admin";
	// Ĭ�϶˿�
	private int port = 8899;
	//�豸ͼ���·��
	private String iconNPath ;
	
    /**
     *�豸��ǰ��״̬  
     * 0:����  1������  2���������
     */
	private int state = 0;
	
	// �Ƿ��Ǳ����豸
	private boolean isLocal = true;
	/**
	 * ���ݽ��յ����������ذ�������������
	 * 
	 * @param data
	 */
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getPasd() {
		return pasd;
	}

	public void setPasd(String pasd) {
		this.pasd = pasd;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isLocal() {
		return isLocal;
	}

	public void setLocal(boolean isLocal) {
		this.isLocal = isLocal;
	}

	/**
	 * �����豸��״̬
	 * @param state  0������   1������   2��������� 
	 */
	public void setState(int state) {
		this.state = state;
	}
	
	/**
	 * 0������   1������   2���������  
	 * @return
	 */
	public int getState() {
		return state;
	}
	
	
	public String getIconNPath() {
		return iconNPath;
	}
	
	public void setIconNPath(String iconNPath) {
		this.iconNPath = iconNPath;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Device){
			Device dev = (Device)o;
			return this.mac.equals(dev.getMac());
		}
		return super.equals(o);
	}
	
	@Override
	public String toString() {
		return "dev to String--------->mac" + mac + " iocon_path:" + iconNPath
				+ " name:" + name + " state:" + state + " ip:" + ip+" isLocal:"+isLocal;
	}
}
