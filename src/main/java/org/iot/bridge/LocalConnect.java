package org.iot.bridge;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;


public class LocalConnect
  extends BaseConnect
{
  public LocalConnect(String host, String mac, String pasd, int port, boolean isLocal)
  {
    super(host, mac, pasd, port, isLocal);
  }
  
  public boolean connect(int timeOut)
  {
    if (isConnect()) {
      return true;
    }
    this.socket = new Socket();
    SocketAddress sa = new InetSocketAddress(this.host, this.port);
    try
    {
      if (this.socket == null) {
        return false;
      }
      try
      {
        this.socket.setKeepAlive(true);
        this.socket.setSoTimeout(5000);
        this.socket.connect(sa, timeOut);
        this.os = this.socket.getOutputStream();
        this.is = this.socket.getInputStream();
        this.close = false;
      }
      catch (NullPointerException e)
      {
        return false;
      }
      if (this.rListener == null) {
        //break label159;
        System.out.println("break");
      }
    }
    catch (IOException e)
    {
      closeConnect();
      if (this.rListener != null) {
        this.rListener.connectBreak("error_001", this.mac);
      }
      e.printStackTrace();
      return false;
    }
    this.rListener.connectSuccess(this.mac);
    //label159:
    //this.stateChecker = new ConnectStateChecker(this.socket, this.mac);
    //start();
    
    //this.stateChecker.start();
    try
    {
      send((this.pasd + "\r\n").getBytes("utf-8"));
    }
    catch (UnsupportedEncodingException e)
    {
      e.printStackTrace();
      closeConnect();
      return false;
    }
    return true;
  }
}