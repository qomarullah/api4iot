package org.iot.bridge;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class StringUtil {

	/**
	 * ���������Ĺؼ�������ָ��
	 * 
	 * @return
	 */
	public static byte[] generateCmd(byte[] key) {
		// ����GPIOЭ�飺��ͷ2 + ���� 2 + ID1 + ���� + ����+ У��1
		int length = 5 + key.length;
		byte[] cmd = new byte[length];
		cmd[0] = 0x55;
		cmd[1] = (byte) 0xaa;

		// GPIOЭ���еĳ���
		byte[] lengthBytes = int2byte(key.length);
		cmd[2] = lengthBytes[1];
		cmd[3] = lengthBytes[0];

		// У��λ�ۼ�
		cmd[length - 1] = (byte) (cmd[2] + cmd[3]);
		for (int i = 0; i < key.length; i++) {
			cmd[i + 4] = key[i];
			cmd[length - 1] += key[i];
		}
		return cmd;
	}

	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv + " ");
		}
		return stringBuilder.toString();
	}

	/**
	 * int תbyte[]
	 * 
	 * @param num
	 * @return
	 */
	public static byte[] int2byte(int res) {
		byte[] targets = new byte[4];
		targets[0] = (byte) (res & 0xff);// ���λ
		targets[1] = (byte) ((res >> 8) & 0xff);// �ε�λ
		targets[2] = (byte) ((res >> 16) & 0xff);// �θ�λ
		targets[3] = (byte) (res >>> 24);// ���λ,�޷������ơ�
		return targets;
	}

	/**
	 * ��4�ֽڵ�byte����ת��intֵ
	 * 
	 * @param b
	 * @return
	 */
	public static int byteArray2int(byte[] b) {
		byte[] a = new byte[4];
		int i = a.length - 1, j = b.length - 1;
		for (; i >= 0; i--, j--) {// ��b��β��(��intֵ�ĵ�λ)��ʼcopy����
			if (j >= 0)
				a[i] = b[j];
			else
				a[i] = 0;// ���b.length����4,�򽫸�λ��0
		}
		int v0 = (a[0] & 0xff) << 24;// &0xff��byteֵ�޲���ת��int,����Java�Զ�����������,�ᱣ����λ�ķ���λ
		int v1 = (a[1] & 0xff) << 16;
		int v2 = (a[2] & 0xff) << 8;
		int v3 = (a[3] & 0xff);
		return v0 + v1 + v2 + v3;
	}

	/**
	 * ��byte[]װ��Ϊ2���Ƶ��ַ����������� ����ʹ�����ַ���
	 * 
	 * @param data
	 * @return
	 */
	public static String bytesToBinaryString(byte[] data) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			int value = data[i] & 0xff;
			String str2 = Integer.toBinaryString(value);
			for (int x = str2.length(); x < 8; x++) {
				str2 = "0" + str2;
			}
			sb.append(new StringBuilder(str2).reverse().toString());
		}
		return sb.toString();
	}

	/**
	 * ��byte[]װ��Ϊ2���Ƶ��ַ���
	 * 
	 * @param data
	 * @return
	 */
	public static String bytesToBinaryString2(byte[] data) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			int value = data[i] & 0xff;
			String str2 = Integer.toBinaryString(value);
			for (int x = str2.length(); x < 8; x++) {
				str2 = "0" + str2;
			}
			sb.append(new StringBuilder(str2));
		}
		return sb.toString();
	}

	/**
	 * ������Ϊ2��byte����ת��Ϊ16λint ������
	 * 
	 * @param res
	 *            byte[]
	 * @return int
	 * */
	public static int byte2int(byte[] res) {
		// һ��byte��������24λ���0x??000000��������8λ���0x00??0000
		int targets = (res[0] & 0xff) | ((res[1] << 8) & 0xff00); // | ��ʾ��λ��
		// 0x80 = 10000000 ��byte ��һ�¿��Եõ���λ����
		int first = res[0] & 0x80;
		if (first > 0)
			return -targets / 2;
		else
			return targets;
	}

	public static boolean isEmpty(String input) {
		if (input == null || "".equals(input))
			return true;

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
				return false;
			}
		}
		return true;
	}

	public static byte[] LongToBytes8(long l) {
		byte abyte0[] = new byte[8];
		abyte0[7] = (byte) (int) (255L & l);
		abyte0[6] = (byte) (int) ((65280L & l) >> 8);
		abyte0[5] = (byte) (int) ((0xff0000L & l) >> 16);
		abyte0[4] = (byte) (int) ((0xffffffffff000000L & l) >> 24);
		int i = (int) (l >> 32);
		abyte0[3] = (byte) (0xff & i);
		abyte0[2] = (byte) ((0xff00 & i) >> 8);
		abyte0[1] = (byte) ((0xff0000 & i) >> 16);
		abyte0[0] = (byte) ((0xff000000 & i) >> 24);
		return abyte0;
	}

	/**
	 * �ж��ַ����Ƿ���ip��ַ
	 * 
	 * @param ip
	 * @return
	 */
	public static boolean isIPAddress(String ip) {
		String reg = "^(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\."
				+ "(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\."
				+ "(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\."
				+ "(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])$";
		return ip.matches(reg);
	}

	public static boolean isMAC(String mac) {
		mac = mac.replace("-", "");
		if (mac.length() != 12)
			return false;
		return true;
	}

	public static String macFormat(String mac) {
		String[] subStrs = new String[6];
		for (int i = 0; i < subStrs.length; i++)
			subStrs[i] = mac.substring(i * 2, i * 2 + 2);
		return (subStrs[0] + "-" + subStrs[1] + "-" + subStrs[2] + "-"
				+ subStrs[3] + "-" + subStrs[4] + "-" + subStrs[5]).toUpperCase(Locale.getDefault());
	}

	public static byte[] macToBytes_Doug(String mac) {
		byte[] bt = new byte[12];
		String pureMac = "";
		try {
			String[] macs = mac.split("-");
			for (int i = 0; i < 6; i++) {
				pureMac = pureMac + macs[i];
			}// ��ȥ��-��
			pureMac = pureMac.toLowerCase();
			for (int i = 0; i < pureMac.length(); i++) {
				bt[i] = (byte) pureMac.charAt(i);
			}

		} catch (NumberFormatException e) {
		}
		return bt;
	}

	public static String macToStr(String mac) {

		String res = "";
		if (mac != null) {
			String[] tmp = mac.split("-");
			if (tmp == null || tmp.length == 0) {
				return mac;
			}
			for (int i = 0; i < tmp.length; i++) {
				res += tmp[i].toLowerCase();
			}
		}
		return res;
	}

	public static String formatSingleTime(int value) {
		String timeStr = "";
		if (value < 10) {
			timeStr = "0" + value;
		} else
			timeStr = String.valueOf(value);
		return timeStr;
	}

	public static long bytes8ToLong(byte abyte0[]) {
		long l = (0xff & abyte0[0]) << 56 | (0xff & abyte0[1]) << 48
				| (0xff & abyte0[2]) << 40 | (0xff & abyte0[3]) << 32
				| (0xff & abyte0[4]) << 24 | (0xff & abyte0[5]) << 16
				| (0xff & abyte0[6]) << 8 | 0xff & abyte0[7];
		return l;
	}

	/**
	 * ������ʹ��
	 * 
	 * @param week
	 * @return
	 */
	
	/**
	 * ���ر�������Դ
	 * 
	 * @param str
	 * @return
	 */
	public static List<Integer> getLockedSource(String str) {
		List<Integer> lockedList = new ArrayList<Integer>();
		if (StringUtil.isEmpty(str))
			return lockedList;
		String[] lockeds = str.split(",");
		for (String indexStr : lockeds) {
			int index = Integer.parseInt(indexStr);
			lockedList.add(index);
		}
		return lockedList;
	}

	public static String newLockedStr(List<Integer> list) {
		String str = "";
		for (Integer index : list)
			str += String.valueOf(index) + ",";
		return str;
	}
}
